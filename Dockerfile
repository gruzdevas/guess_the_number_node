FROM node

# Создать директорию app
WORKDIR /app

# Установить зависимости приложения
COPY package*.json ./

RUN npm install

# Скопировать исходники приложения
COPY src /app

EXPOSE 3000
CMD [ "node", "app.js" ]