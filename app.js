'use strict';
// Подключаем библиотеки
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const path = require('path');
const GameController = require('./game-controller');

app.use(express.static(path.join(__dirname, 'public'))); // Путь к статичным файлам

app.get('/', (request, response) => {
  response.sendFile('index');
}); // При обращении к сайту, пользователю отправится шаблон index.html

// Создаем главный контроллер
const gameController = new GameController();
gameController.listen(io);

const SERVER_PORT = process.env.PORT || 3000;
app.set('port', SERVER_PORT);

// Запускаем сервер express на порту 3000
server.listen(app.get('port'), () => {
    console.log('Express server listening on port %d in %s mode', app.get('port'), app.get('env'));
});


module.exports = app