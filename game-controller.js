'use strict';

// Модель игрока, чтобы хранить попытки и само число
class Player {
    constructor(id){
        this.id = id;
        this.tries = 0;
        let num = ''; // Генерация случайной строки из 4х символов (цифр)
        for (let i = 0; i < 4; i++)
            num += Math.floor(Math.random() * 10);
        this.number = num;
    }
}

class GameController {

    constructor() {
        this._players = new Map();
    }
    listen(io){
        this.sockets = io.sockets;
        const self = this;
        io.sockets.on('connection', socket => { // Роутинг запросов
            socket.on('start', self.createPlayer.bind(self, socket)),
            socket.on('check', self.checkGuess.bind(self, socket))
        });
    }
    createPlayer(socket){
        const player = new Player(socket.id);
        this._players.set(player.id, player);
    }
    checkGuess(socket, number){ // Проверка числа на соответствие. Проблема в том, что если число будет, 
        let player = this._players.get(socket.id); // например, 1110, а у пользователя 0000, то первые
        let answer = 'ХХХХ'; // 3 пометятся как К (что теоретически правильно при повторяющихся цифрах)
        let ansArray = answer.split('');
        if (number.length != 4 || isNaN(number)){ // Проверка корректности данных
            let num = '';
            for (let i = 0; i < 4; i++)
                num += Math.floor(Math.random() * 10);
                number = num;
        }
        for (let i = 0; i < number.length; i++){
            if (number.charAt(i) == player.number.charAt(i)){
                ansArray[i] = 'В';
            }
            else if (player.number.includes(number.charAt(i))){
                ansArray[i] = 'К';
            }
        }
        answer = ansArray.join('');
        player.tries += 1;
        const playerSocket = this.sockets.connected[player.id];
        if (playerSocket) {
            if (answer == 'ВВВВ')
                playerSocket.emit('win', answer, number, player.tries); // Если пользователь угадал, посылаем запрос win
            else
                playerSocket.emit('checked', answer, number, player.tries); // Если нет, то checked
        }
    }
}

module.exports = GameController;