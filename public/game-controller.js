export default class GameController {
    constructor() {
        this.getGuessField().addEventListener('keyup', this._validateInput.bind(this)); // Отслеживаем нажатие кнопок, и изменение текста в форме
        this.getGuessButton().addEventListener('click', this._handleButtonClick.bind(this));
        this.getRestartButton().addEventListener('click', this._reloadGame.bind(this));
    }

    connect(io) {
        this.socket = io();
        this._initializeSocketIoHandlers();
        this.socket.emit("start"); 
    } // При подключении посылаем запрос start, чтобы создать пользователя

    _initializeSocketIoHandlers() {
        this.socket.on('checked', this.updateResults.bind(this));
        this.socket.on('win', this.showWinAlert.bind(this));
    }

    updateResults(result, number, tries){
        this.getResultTable().innerHTML = `<th scope="row">${tries}</th> <td> ${number} </td> <td> ${result} </td>`+ this.getResultTable().innerHTML;
    } // Обновление таблицы

    declOfNum(number, titles) {  
        let cases = [2, 0, 1, 1, 1, 2];  
        return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
    } // Склонение слова

    showWinAlert(result, number, tries) {
        this.updateResults(result, number, tries);
        this.getWinAlert().innerHTML = `<p>Вы угадали число за ${tries} ${this.declOfNum(tries, ['попытка','попытки','попыток'])}. Чтобы начать заново нажмите кнопку выше.</p>`;
        this.getGuessForm().hidden = true;
        this.getWinAlert().hidden = false;
        this.getRestartButton().hidden = false;
    } // Показ алерта и кнопки рестарта, скрытие формы

    /** Функции для доступа к элементам html */
    getGuessButton() {
        return document.getElementById('guessButton');
    }

    getGuessField() {
        return document.getElementById('guessField');
    }

    getGuessForm() {
        return document.getElementById('guessForm');
    }

    getRestartButton() {
        return document.getElementById('restartButton');
    }

    getWinAlert() {
        return document.getElementById('winAlert');
    }

    getResultTable() {
        return document.getElementById('resultTable');
    }

    _handleButtonClick(e){
        e.preventDefault();
        this.socket.emit('check', this.getGuessField().value);
        this.getGuessField().value='';
        this.getGuessField().focus();
    } // Обработка нажатия кнопки угадать

    _reloadGame(){
        location.reload();
    } // Перезагрузка страницы при нажатии кнопки начать заново

    _validateInput(){
        let isValid = this.getGuessField().checkValidity();
        if ( isValid ) {
            this. getGuessButton().disabled = false;
        } else {
            this. getGuessButton().disabled = true;
        }
    } // Валидация поля ввода


}